# woodhouse-integration-test

For testing Woodhouse - GitLab integration.

- Incident issues created by `/woodhouse incident declare`
- Issue event webhooks to Slack messages.